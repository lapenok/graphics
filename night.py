#! /usr/bin/python3

import argparse, os, random, sys
from mutagen.mp3 import MP3
from pygame import mixer
from tkinter import Canvas, Tk

SOURCE_PATH = os.path.abspath(os.path.dirname(__file__))
SPEED_LIST = [10, 5, 2]
HEIGHT_LIST = [10, 150, 250]
TIMER_DELAY = 2000

parser = argparse.ArgumentParser(
    description='Example of program which draw a picture \
        with animation using the library Tkinter. Use the \
        key Escape for exit',
    epilog='Veronika Lapenok <veronika.lapenok@mail.ru>')

parser.add_argument('--version', action='version', version='%(prog)s 1.0')
parser.add_argument('--usage', action='store_true', dest='isUsage', 
        help='show this usage message and exit' )

results = parser.parse_args()

if results.isUsage:
    parser.print_usage()
    sys.exit(0)

# Returns coordinates of rectangle
# minX, minY - left upper coordinates (type: float)
# maxX, maxY - right down coordinates (type: float)
# raw - array of raw data (type: list)
class RectCoords():
        def __init__(self, minX, minY, maxX, maxY):
                self.minX = minX
                self.minY = minY
                self.maxX = maxX
                self.maxY = maxY
                self.raw = [minX, minY, maxX, maxY]

# Returns coordinates of rectangle
# polygon - polygon of points
# speed - speed of the ufo flying 
# isForward - direction of the ufo flying (type - boolean)
class Ufo():
        def __init__(self, polygon, speed, isForward = True):
                self.polygon = polygon
                self.speed = speed
                self.isForward = isForward

# Draw the ufo
# Input:
#       pointX, pointY - the coordinate of the left top point ufo (types: float)
#       isForward - flag of the direction drawing ufo from left to right(default - True), (type - boolean) 
# Output: the object 'ufo'
def drawUfo(pointX, pointY, isForward = True):
        if isForward:
                coef = 1
                offset = 0
        else:
                coef = -1  
                offset = 60
        ufo = canvas.create_polygon(pointX + offset, pointY, pointX + 40 * coef + offset, 
                pointY, pointX + 60 * coef + offset, pointY + 10, pointX + 40 * coef + offset, 
                pointY + 20, pointX + offset, pointY + 20, pointX + 10 * coef + offset, 
                pointY + 15, pointX + offset, pointY + 10, pointX + 10 * coef + offset, 
                pointY + 5, fill='gray')
        return ufo

# Gets coordinates of the rectangle
# Input: points of the polygon (type: float)
# Output: object "rectCoords" of the class "RectCoords"(type: float)
def getRectCoords(polygon):
        coords = canvas.coords(polygon)

        minX = None
        minY = None
        maxX = None
        maxY = None
        for i in range(len(coords)):
                if i % 2 == 0:
                        # Axis X
                        if minX is None or coords[i] < minX:
                                minX = coords[i]
                        elif maxX is None or coords[i] > maxX: 
                                maxX = coords[i]
                else:
                        # Axis Y
                        if minY is None or coords[i] < minY:
                                minY = coords[i]
                        elif maxY is None or coords[i] > maxY:
                                maxY = coords[i]

        return RectCoords(minX, minY, maxX, maxY)

# Randomly selects the flight height and speed of the object 'ufo'
# Input: list of the values (type: list)
# Output: random value (type: int)
def getRandValue(values, excludValue = None):
        value = excludValue
        
        while value == excludValue:
                value = random.choice(values)
        return value

# Cyclically moves object 'ufo'
# Input: object of the Ufo class (type: object)
# Output: void
def flyUfo(ufo):
        rect = getRectCoords(ufo.polygon)
        if ufo.isForward:
                if rect.minX <= (float(canvas['width'])):
                        canvas.move(ufo.polygon, 1, 0)
                        tk.after(ufo.speed, lambda: flyUfo(ufo))
                elif rect.minX > (float(canvas['width'])):
                        ufo.speed = getRandValue(SPEED_LIST, ufo.speed)
                        ufo.isForward = False
                        ufo.polygon = drawUfo(rect.minX, getRandValue(HEIGHT_LIST, rect.minY), ufo.isForward)
                        tk.after(TIMER_DELAY, lambda: flyUfo(ufo))
        else:
                if rect.maxX >= 0:
                        canvas.move(ufo.polygon, -1, 0)
                        tk.after(ufo.speed, lambda: flyUfo(ufo))
                elif rect.maxX < 0:
                        ufo.speed = getRandValue(SPEED_LIST, ufo.speed)
                        ufo.isForward = True
                        ufo.polygon = drawUfo(rect.minX, getRandValue(HEIGHT_LIST, rect.minY), ufo.isForward)
                        tk.after(TIMER_DELAY, lambda: flyUfo(ufo))

def onKeyPress(event):
        if event.keysym == "Escape":
                mixer.quit()
                sys.exit(0)

# Plays a music file
# Input: music file (type: mp3)
# Output: void
# Made for example (for simplified loop audio playback use: mixer.music.play(-1))
def playSound(sound):
        mixer.music.load(sound) 
        mixer.music.play()
        song = MP3(sound)
        songLength = int(song.info.length)
        if mixer.music.get_busy() == True:
                tk.after((songLength * 1000 + 1000), lambda: playSound(sound))

tk = Tk()
canvas = Canvas(tk, width=500, height=500, bg="black")

tk.title(os.path.basename(sys.argv[0]))
tk.resizable(width=False, height=False)

canvas.pack()

mixer.init()

# Draw the moon
canvas.create_oval(40, 10, 90, 60, fill='yellow')
# Draw the Big bear
canvas.create_polygon(205, 110, 210, 115, 205, 120, 200, 115, fill='yellow2')
canvas.create_polygon(245, 115, 250, 120, 245, 125, 240, 120, fill='yellow2')
canvas.create_polygon(275, 140, 280, 145, 275, 150, 270, 145, fill='yellow2')
canvas.create_polygon(305, 160, 310, 165, 305, 170, 300, 165, fill='yellow2')
canvas.create_polygon(315, 200, 320, 205, 315, 210, 310, 205, fill='yellow2')
canvas.create_polygon(375, 210, 380, 215, 375, 220, 370, 215, fill='yellow2')
canvas.create_polygon(385, 160, 390, 165, 385, 170, 380, 165, fill='yellow2')

# Draw the house
# Draw the wall
canvas.create_rectangle(150, 380, 310, 450, fill='bisque')
canvas.create_line(250, 380, 250, 450)
# Draw the roof
canvas.create_polygon(285, 330, 250, 380, 310, 380, fill='dark goldenrod', 
        outline='black')
canvas.create_polygon(190, 330, 285, 330, 250, 380, 150, 380, 
        fill='dark goldenrod')
# Draw the window
canvas.create_rectangle(200, 400, 230, 430, fill='lemon chiffon', outline='black')
canvas.create_line(215, 400, 215, 430)
# Draw the door
canvas.create_rectangle(270, 410, 290, 450, fill='blue')
canvas.create_oval(285, 430, 285, 430)
# Draw the trumpet
canvas.create_rectangle(220, 310, 240, 345, fill='PeachPuff4')
canvas.create_polygon(215, 345, 245, 345, 250, 350, 210, 350, fill='black')

# Draw the ground
canvas.create_rectangle(0, 450, 500, 500, fill='brown')

flyUfo(Ufo(drawUfo(-60, getRandValue(HEIGHT_LIST)), getRandValue(SPEED_LIST)))

playSound(SOURCE_PATH + '/sounds/sound.mp3')

canvas.bind("<KeyPress>", onKeyPress)
canvas.focus_set()

tk.mainloop()