#! /usr/bin/python3

import argparse, os, sys
from tkinter import *

parser = argparse.ArgumentParser(
    description='Program which draw geometric shapes for the \
        given coordinates using the library Tkinter,', 
    epilog = 'Veronika Lapenok <veronika.lapenok@mail.ru>')

parser.add_argument('--version', action='version', version='%(prog)s 1.0')
parser.add_argument('--usage', action='store_true', dest='isUsage', 
    help='show this usage message and exit' )

results = parser.parse_args()

if results.isUsage == True:
    parser.print_usage()
    sys.exit(0)

tk = Tk()
tk.title(os.path.basename(sys.argv[0]))

c = Canvas(tk, width = 500, height = 500, bg = 'white')
c.pack()

c.create_polygon([10, 10], [60, 10], [80, 80], fill = "blue")
c.create_rectangle(100, 100, 190, 170, fill = "red")
c.create_polygon([220, 220], [270, 220], [290, 240], [270, 260], [220, 260], 
    [220, 220], fill = "green")
c.create_polygon([300, 280], [320, 300], [320, 320], [300, 340], [280, 320], 
    [280, 300], fill = "pink")

tk.mainloop()