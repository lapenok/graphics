#! /usr/bin/python3

import argparse, os, sys, turtle

parser = argparse.ArgumentParser(
    description='Program which draw geometric shapes for the \
        given coordinates using the library Turtle', 
    epilog='Veronika Lapenok <veronika.lapenok@mail.ru>')

parser.add_argument('--version', action='version', version='%(prog)s 1.0')
parser.add_argument('--usage', action='store_true', dest='isUsage', 
    help='show this usage message and exit' )

results = parser.parse_args()

if results.isUsage == True:
    parser.print_usage()
    sys.exit(0)

turtle.title(os.path.basename(sys.argv[0]))

turtle.reset()

# a) drew a triangle on the given coordinates
turtle.up()
turtle.begin_fill()
turtle.color("aquamarine")
turtle.goto(-100, -100)
turtle.down()
turtle.goto(-150, -100)
turtle.goto(-80, -170)
turtle.goto(-100, -100)
turtle.up()
turtle.end_fill()
turtle.forward(100)


# b) drew a rectangle on the given coordinates
turtle.up()
turtle.begin_fill()
turtle.color("yellow")
turtle.goto(-10, -10)
turtle.down()
turtle.goto(-100, -10)
turtle.goto(-100, -80)
turtle.goto(-10, -80)
turtle.goto(-10, -10)
turtle.up()
turtle.end_fill()
turtle.forward(100)

# c) drew a pentagon on the given coordinates
turtle.up()
turtle.begin_fill()
turtle.color("purple")
turtle.goto(10, 10)
turtle.down()
turtle.goto(60, 10)
turtle.goto(80, 30)
turtle.goto(60, 50)
turtle.goto(10, 50)
turtle.goto(10, 10)
turtle.up()
turtle.end_fill()
turtle.forward(100)

# d) drew a hexagon on the given coordinates
turtle.up()
turtle.begin_fill()
turtle.color("lime green")
turtle.goto(100, 80)
turtle.down()
turtle.goto(120, 100)
turtle.goto(120, 120)
turtle.goto(100, 140)
turtle.goto(80, 120)
turtle.goto(80, 100)
turtle.goto(100, 80)
turtle.up()
turtle.end_fill()
turtle.forward(100)

turtle.exitonclick()