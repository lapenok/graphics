# Graphics

Author: Veronika Lapenok<br>
Mail: <veronika.lapenok@mail.ru><br>
License: LGPL 3.0

Graphics is a project that contains examples of scripts in the Python3<br> 
programming language using the Turtle and Tkinter graphic libraries.

1. turtle-example.py
the program is executed according to the following condition:<br>
**a)** draw a triangle;<br>
**b)** draw a rectangle;<br>
**c)** draw pentagon;<br>
**d)** draw hexagon.<br>
<img src="/images/turtle-example.png" alt="drawing" width="300"/>

2. tkinter-example.py
the program is executed according to the following condition:<br>
**a)** draw a triangle;<br>
**b)** draw a rectangle;<br>
**c)** draw pentagon;<br>
**d)** draw hexagon.<br>
<img src="/images/tkinter-example.png" alt="drawing" width="300"/>

3. shape.py
The program draws a complex volumetric shape using the Tkinter graphic library.<br> 
The location of the shape on the canvas can be specified using command<br> 
line arguments. Default coordinates are given as pointX = 500, pointY = 500.<br>
<img src="/images/shape.png" alt="drawing" width="300"/>

4. girl.py
The program draws a multiplication girl with animation using the Tkinter graphic library.<br> 
To perform the animation, you must use the keys "Up", "Down", "Left" and "Right" for the<br> 
girl's hands move in a given direction.<br>
<img src="/images/girl.png" alt="drawing" width="300"/>

5. night.py
The example of program with animation using the Tkinter graphic library.<br> 
For exit press Escape.
<img src="/images/night.png" alt="drawing" width="300"/>

For more information use the help (--help or -h)

In case of an error "ModuleNotFoundError: No module named<br> 
'tkinter'" command to be executed "sudo apt-get install<br> 
python3-tk".


