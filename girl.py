#! /usr/bin/python3

import argparse, os, sys
from tkinter import *

parser = argparse.ArgumentParser(
    description='Program which draw multiplication girl \
        with animation using the library Tkinter. To \
        start the animation please press: ← → ↓ ↑ ', 
    epilog='Veronika Lapenok <veronika.lapenok@mail.ru>')

parser.add_argument('--version', action='version', version='%(prog)s 1.0')
parser.add_argument('--usage', action='store_true', dest='isUsage', 
    help='show this usage message and exit' )

results = parser.parse_args()

if results.isUsage == True:
    parser.print_usage()
    sys.exit(0)

tk = Tk()
tk.title(os.path.basename(sys.argv[0]))

canvas = Canvas(tk, width = 500, height = 500, bg = 'white')
canvas.pack()

def drawBody():

    #body/dress
    canvas.create_polygon(250, 260, 200, 380, 300, 380, fill = "coral")
    canvas.create_oval(250, 280, 260, 290, outline = "yellow", fill = "yellow")
    canvas.create_oval(240, 300, 250, 310, outline = "blue", fill = "blue")
    canvas.create_oval(230, 340, 250, 360, outline = "red", fill = "red")
    canvas.create_oval(260, 330, 280, 350, outline = "yellow", fill = "yellow")
    canvas.create_oval(210, 360, 220, 370, outline = "yellow", fill = "yellow")
    canvas.create_oval(260, 370, 270, 378, outline = "blue", fill = "blue")
    canvas.create_oval(280, 360, 290, 370, outline = "red", fill = "red")
    #head
    canvas.create_oval(220, 190, 280, 270, outline = "white", fill = "light goldenrod")
    #legs
    canvas.create_rectangle(220, 380, 230, 410, outline = "white", 
        fill = "light goldenrod")
    canvas.create_rectangle(270, 380, 280, 410, outline = "white", 
        fill = "light goldenrod")
    #feet
    canvas.create_oval(210, 410, 230, 420, fill = "black")
    canvas.create_oval(270, 410, 290, 420, fill = "black")
    #hair
    canvas.create_arc(210, 180.5, 290, 270, start=0, extent=180, outline="brown", 
        fill="brown")
    canvas.create_line(215, 220, 215, 270, fill = "brown", width = 11)
    canvas.create_line(285, 220, 285, 270, fill = "brown", width = 12)
    canvas.create_polygon(220, 200, 230, 210, 230, 200, 230, 190, 240, 200, 
        fill = "yellow")
    #face
    canvas.create_oval(243, 230.8, 239, 237)
    canvas.create_oval(261, 230.8, 257, 237)
    canvas.create_oval(240.45, 234.5, 240.45, 234.5)
    canvas.create_oval(258.3, 234.5, 258.3, 234.5)
    canvas.create_line(250, 240.5, 247, 250)
    canvas.create_line(250, 250, 247, 250)
    canvas.create_arc(243, 257, 257, 264, start = 180, extent = 180, outline="red", 
        fill="red")

#sweet
def drawSweet(pointX, pointY):
    canvas.create_oval(pointX, pointY, pointX + 20, pointY + 20, fill = "orange red")
    
    step = 5
    point = [pointX + 9, pointY + 9, pointX + 20 - 9, pointY + 20 - 9]
    iters = 0
    canvas.create_arc(*point, start = 0, extent = 180, style = ARC)
    while iters < 4:
        point[1] -= step/2  #expansion down
        point[3] += step/2  #expansion up
  
        if iters % 2:
            point[0] -= step  # expanding and shifting the left border to the left
            canvas.create_arc(*point, start=0, extent=180, style=ARC)
        else:
            point[2] += step  # expansion and shift of the right border to the right
            canvas.create_arc(*point, start=0, extent=-180, style=ARC)
  
        iters += 1

#hands
def drawBottomHands():
    canvas.create_line(240, 280, 190, 330, width = 5, fill = "light goldenrod")
    canvas.create_line(260, 280, 310, 330, width = 5, fill = "light goldenrod")
    
    canvas.create_line(175, 295, 210, 330, width = 2, fill = "sienna4")
    drawSweet(160, 280)
    
def drawBottomLeftHands():
    canvas.create_line(240, 280, 180, 320, width = 5, fill = "light goldenrod")
    canvas.create_line(260, 280, 200, 330, width = 5, fill = "light goldenrod")

    canvas.create_line(200, 320, 165, 285, width = 2, fill = "sienna4")
    drawSweet(150, 270)
    
def drawBottomRightHands():
    canvas.create_line(240, 280, 300, 330, width = 5, fill = "light goldenrod")
    canvas.create_line(260, 280, 330, 320, width = 5, fill = "light goldenrod")

    canvas.create_line(280, 330, 315, 295, width = 2, fill = "sienna4")
    drawSweet(310, 280)
   
def drawUpperHands():
    canvas.create_line(190, 210, 240, 280, width = 5, fill = "light goldenrod")
    canvas.create_line(310, 210, 260, 280, width = 5, fill = "light goldenrod")

    canvas.create_line(200, 220, 165, 185, width = 2, fill = "sienna4")
    drawSweet(150, 170)
    
def drawUpperLeftHands():
    canvas.create_line(240, 280, 170, 230, width = 5, fill = "light goldenrod")
    canvas.create_line(260, 280, 210, 220, width = 5, fill = "light goldenrod")

    canvas.create_line(180, 240, 145, 205, width = 2, fill = "sienna4")
    drawSweet(130, 190)
       
def drawUpperRightHands():
    canvas.create_line(240, 280, 290, 220, width = 5, fill = "light goldenrod")
    canvas.create_line(260, 280, 330, 230, width = 5, fill = "light goldenrod")

    canvas.create_line(280, 230, 315, 195, width = 2, fill = "sienna4")
    drawSweet(310, 180)
    
drawBody()
drawBottomHands()

isBottomHands = True

def onKeyPress(event):
    global isBottomHands
    
    if event.keysym == "Up":
        canvas.delete("all")
        drawBody()
        drawUpperHands()
        isBottomHands = False
    elif event.keysym == "Down":
        canvas.delete("all")
        drawBody()
        drawBottomHands()
        isBottomHands = True
    elif event.keysym == "Left" and  isBottomHands == False: 
        canvas.delete("all")
        drawBody()
        drawUpperLeftHands()   
    elif event.keysym == "Right" and  isBottomHands == False:
        canvas.delete("all")
        drawBody()
        drawUpperRightHands()
    elif event.keysym == "Left" and  isBottomHands == True:
        canvas.delete("all")
        drawBody()
        drawBottomLeftHands()
    elif event.keysym == "Right" and isBottomHands == True:
        canvas.delete("all")
        drawBody()
        drawBottomRightHands()
      
canvas.bind("<KeyPress>", onKeyPress)
canvas.focus_set()

tk.mainloop()


