#! /usr/bin/python3

import argparse, os, sys
from tkinter import *

parser = argparse.ArgumentParser(
    description='Program, which draw difficult geometric shape \
        using the library Tkinter.', 
    epilog='Veronika Lapenok <veronika.lapenok@mail.ru>')

parser.add_argument('--version', action='version', version='%(prog)s 1.0')
parser.add_argument('--usage', action='store_true', dest='isUsage', 
    help='show this usage message and exit' )
parser.add_argument('-x', dest='pointX', type=int, default=250, 
    help='set X-coordinate for rhe drowing shape')
parser.add_argument('-y', dest='pointY', type=int,  default=250, 
    help='set Y-coordinate for rhe drowing shape')

results = parser.parse_args()

if results.isUsage == True:
    parser.print_usage()
    sys.exit(0)

pointX = results.pointX
pointY = results.pointY

tk = Tk()
tk.title(os.path.basename(sys.argv[0]))

canvas = Canvas(tk, width = 500, height = 500, bg = 'white')
canvas.pack()

def drowShape(pointX, pointY):
    # front - bottom
    canvas.create_rectangle(pointX, pointY + 20, pointX - 20, pointY + 60, 
    outline = "grey", fill = "white")
    # right - bottom
    canvas.create_polygon([pointX, pointY + 20],
        [pointX + 10, pointY + 10],
        [pointX + 10, pointY + 50],
        [pointX, pointY + 60],
        outline = "grey",
        fill = "white")

    # right - right
    canvas.create_polygon([pointX + 10, pointY - 10],
        [pointX + 40, pointY - 40],
        [pointX + 40, pointY - 20],
        [pointX + 10, pointY + 10],
        outline = "grey",
        fill = "white")
    # front - left
    canvas.create_rectangle(pointX - 20, pointY, pointX - 60, pointY + 20, 
    outline = "grey", fill = "white")
    # front - front
    canvas.create_polygon([pointX - 50, pointY + 30],
        [pointX - 30, pointY + 30],
        [pointX - 30, pointY + 50],
        [pointX - 50, pointY + 50],
        outline = "grey",
        fill = "white")
    # right - left
    canvas.create_polygon([pointX, pointY],
        [pointX, pointY + 20],
        [pointX - 30, pointY + 50],
        [pointX - 30, pointY + 30],
        outline = "grey",
        fill = "white")
    # front - right
    canvas.create_rectangle(pointX, pointY, pointX + 40, pointY + 20, 
    outline = "grey", fill = "white")
    # right - front
    canvas.create_polygon([pointX + 40, pointY],
        [pointX + 50, pointY - 10],
        [pointX + 50, pointY + 10],
        [pointX + 40, pointY + 20],
        outline = "grey",
        fill = "white")

    # top - back
    canvas.create_polygon([pointX + 10, pointY - 10],
        [pointX - 10, pointY - 10],
        [pointX + 20, pointY - 40],
        [pointX + 40, pointY - 40],
        outline = "grey",
        fill = "grey")
    # top - left
    canvas.create_polygon([pointX - 60, pointY],
        [pointX - 50, pointY - 10],
        [pointX - 10, pointY - 10],
        [pointX - 20, pointY],
        outline = "grey",
        fill = "grey")
    # top - front
    canvas.create_polygon([pointX - 20, pointY],
        [pointX, pointY],
        [pointX - 30, pointY + 30],
        [pointX - 50, pointY + 30],
        outline = "grey",
        fill = "grey")
    # top - right
    canvas.create_polygon([pointX, pointY],
        [pointX + 10, pointY - 10],
        [pointX + 50, pointY - 10],
        [pointX + 40, pointY],
        outline = "grey",
        fill = "grey")

    # front - top
    canvas.create_rectangle(pointX, pointY, pointX - 20, pointY - 40, 
    outline = "grey", fill = "white")
    # right - top
    canvas.create_polygon([pointX, pointY - 40],
        [pointX + 10, pointY - 50],
        [pointX + 10, pointY - 10],
        [pointX, pointY],
        outline = "grey",
        fill = "white")
    
    # top - top
    canvas.create_polygon([pointX - 20, pointY - 40],
        [pointX - 10, pointY - 50],
        [pointX + 10, pointY - 50],
        [pointX, pointY - 40],
        outline = "grey",
        fill = "grey")

# bottom
drowShape(pointX, pointY + 100)
# left
drowShape(pointX - 100, pointY)
# back
drowShape(pointX + 70, pointY - 70)

# middle
drowShape(pointX, pointY)

# upper
drowShape(pointX, pointY - 100)
# right
drowShape(pointX + 100, pointY)
# front
drowShape(pointX - 70, pointY + 70)

# patches
# top - bottom
canvas.create_line(pointX - 20 + 1, pointY - 40, pointX, pointY - 40, fill="white")
canvas.create_rectangle([pointX + 1, pointY - 40 + 1],
    [pointX + 10 - 1, pointY - 50 - 1],
    outline = "white",
    fill = "white")
# left - right
canvas.create_line(pointX + 40, pointY + 1, pointX + 40, pointY + 20, fill = "white")
# back - front
canvas.create_line(pointX - 30, pointY + 30 + 1, pointX - 30, pointY + 50, fill = "white")

# bottom - top
canvas.create_line(pointX - 20 + 1, pointY + 60, pointX, pointY + 60, fill="white")
canvas.create_rectangle([pointX + 1, pointY + 60 + 1],
    [pointX + 10 - 1, pointY + 50 - 1],
    outline = "white",
    fill = "white")
# right - left
canvas.create_line(pointX - 60, pointY + 1, pointX - 60, pointY + 20, fill = "white")
# front - back
canvas.create_line(pointX + 40, pointY - 40 + 1, pointX + 40, pointY - 20, fill = "white")

tk.mainloop()